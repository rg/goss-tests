#!/bin/bash

# Install
if [[ ! `which goss` ]]
then
    curl -fsSL https://goss.rocks/install | sudo sh
fi

# Test
goss validate -f nagios_verbose

